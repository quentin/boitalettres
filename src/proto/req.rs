use imap_codec::types::command::Command;

pub type Request = Command;
