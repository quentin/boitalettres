use boitalettres::proto::{Request, Response};
use boitalettres::server::accept::addr::{AddrIncoming, AddrStream};
use boitalettres::server::Server;

async fn handle_req(req: Request) -> eyre::Result<Response> {
    use imap_codec::types::response::Status;

    tracing::debug!("Got request: {:#?}", req);

    Ok(Response::Status(
        Status::ok(Some(req.tag), None, "Ok").map_err(|e| eyre::eyre!(e))?,
    ))
}

#[tokio::main]
async fn main() -> eyre::Result<()> {
    setup_logging();

    let incoming = AddrIncoming::new("127.0.0.1:4567").await?;

    let make_service = tower::service_fn(|addr: &AddrStream| {
        tracing::debug!(remote_addr = %addr.remote_addr, local_addr = %addr.local_addr, "accept");

        let service = tower::ServiceBuilder::new()
            .buffer(16)
            .service_fn(handle_req);

        futures::future::ok::<_, std::convert::Infallible>(service)
    });

    let server = Server::new(incoming).serve(make_service);
    let _ = server.await?;

    Ok(())
}

// Don't mind this, this is just for debugging.
fn setup_logging() {
    use tracing_subscriber::prelude::*;

    tracing_subscriber::registry()
        .with(console_subscriber::spawn())
        .with(
            tracing_subscriber::fmt::layer().with_filter(
                tracing_subscriber::filter::Targets::new()
                    .with_default(tracing::Level::DEBUG)
                    .with_target("boitalettres", tracing::Level::TRACE)
                    .with_target("simple", tracing::Level::TRACE)
                    .with_target("tower", tracing::Level::TRACE)
                    .with_target("tokio_tower", tracing::Level::TRACE)
                    .with_target("mio", tracing::Level::TRACE),
            ),
        )
        .init();
}
